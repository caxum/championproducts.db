﻿CREATE TABLE [dbo].[InvoiceDetail] (
    [InvoiceDetailId] INT             IDENTITY (1, 1) NOT NULL,
    [InvoiceId]       INT             NOT NULL,
    [ProductId]       INT             NOT NULL,
    [Amount]          DECIMAL (18, 2) NOT NULL,
    [CreatedDate]     DATETIME        CONSTRAINT [DF_InvoiceDetail_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [DeletedDate]     DATETIME        NULL
);

