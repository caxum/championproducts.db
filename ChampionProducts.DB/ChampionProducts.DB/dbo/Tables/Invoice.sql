﻿CREATE TABLE [dbo].[Invoice] (
    [InvoiceId]     INT             IDENTITY (1, 1) NOT NULL,
    [CustomerId]    INT             NOT NULL,
    [InvoiceAmount] DECIMAL (18, 2) NOT NULL,
    [PaidDate]      DATETIME        NULL,
    [CreatedDate]   DATETIME        CONSTRAINT [DF_Invoice_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [DeletedDate]   DATETIME        NULL
);

