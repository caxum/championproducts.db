﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]     INT           IDENTITY (1, 1) NOT NULL,
    [CustomerTypeId] INT           NOT NULL,
    [Businessname]   VARCHAR (200) NOT NULL,
    [LastName]       VARCHAR (50)  NULL,
    [FirstName]      VARCHAR (50)  NULL,
    [AddressId]      INT           NULL,
    [CreatedDate]    DATETIME      CONSTRAINT [DF_Customer_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]      BIT           NOT NULL, 
    [DeletedDate] DATETIME NULL
);

