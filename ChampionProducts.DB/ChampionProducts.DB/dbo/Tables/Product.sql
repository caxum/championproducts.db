﻿CREATE TABLE [dbo].[Product] (
    [ProductId]   INT           IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (100) NOT NULL,
    [CreatedDate] DATETIME      CONSTRAINT [DF_Product_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]   BIT           NOT NULL
);

